/*
 version: 1.3
 */

var assert = require('assert'),
    util = require("util"),
    path = require("path"),

    _ = require("underscore"),
    smpp = require('smpp'),
    amqp = require("amqp"),
    async = require("async"),
    winston = require("winston"),

    MongoQuery = require('./query').MongoQuery,
    ObjectID = require("mongodb").ObjectID,

    cfg = require('./config'),
    helper = require("./helper")
    ;


var DEBUG = true;

filename = path.basename(__filename, '.js');

//winston.handleExceptions(new winston.transports.File({ filename: path.join(__dirname, "log", filename + "_exception.log"), timestamp: true }));

var transports = [
    new (winston.transports.File)({
        filename: path.join(__dirname, "log", filename + '.log'),
        options: {
            flags: "a"
        },
        level: "debug",
        json: false,
        timestamp: true
    })
];

if (DEBUG) {
    transports.push(new (winston.transports.Console)({ level: "debug", json: false, timestamp: true }));
}

var logger = new (winston.Logger)({
    transports: transports
});


var CONST = {
    PORT: 3775,
    ERRORS: {
        FORBIDDEN: 0xbb,
        ESME_RMSGQFUL: 0x14
    },
    MESSAGE_STATE: {
        REJECTED: 8
    },
    DEFAULT_LIMIT_PER_SECOND: 30,
    SOURCE_ADDR_MAX_LENGTH: 11,
    DEFAULT_GATEWAY: 10
};

function Server() {
    var self = this;

    self.cfg = cfg;
    self.userSession = {};
    self.listening = false;
    self.query = new MongoQuery(self.cfg.MONGO_QUERY);
}


Server.generateObjectId = function () {
    return new ObjectID();
};


Server.prototype.get_rabbit_queue = function (user_id, cb) {
    var _prefix = "node_smpp_server-";
    this.rabbit.queue(_prefix + user_id, {autoDelete: false, durable: true}, function (queue) {
        cb(null, queue);
    });
};


Server.prototype.bind_user_to_queue = function (session, storage, queue) {
    var self = this,
        user_id = storage.user._id;

    var routing_key = user_id + ".#";

    queue
        .on("queueBindOk", function () {

            session.logger.info(util.format("Rabbit subscribed to exchange [%s]", self.EXCHANGE_NAME));

            queue.subscribe({ack: true, prefetchCount: 1}, function (message) {
                session.logger.info(util.format("rabbit > deliver_sm > %s", JSON.stringify(message)));

                if (storage.state === "BOUND_TRX") {
                    session.send(self.get_deliver_sm_pdu(message.short_message, message.number, message.source_addr, message.message_state, message._id));
                    queue.shift();
                } else {
                    queue.close();
                }
            });

        })
        .on("queueUnbindOk", function () {
            console.log('asd');
            session.logger.info("user:" + user_id + " action: unbindOK");
        });

    queue.bind(self.exchange, routing_key);
};


Server.prototype.init = function () {
    var self = this;

    self.EXCHANGE_NAME = "deliver_sm";

    self.rabbit = amqp.createConnection(self.cfg.AMQP, {
        reconnect: true,
        reconnectBackoffStrategy: "exponential",
        reconnectBackoffTime: 1000
    });

    self.rabbit
        .on("ready", function () {

            logger.info("Rabbit Ready");

            self.rabbit.exchange(self.EXCHANGE_NAME, {type: 'topic', autoDelete: false, durable: true}, function (exchange) {
                self.exchange = exchange;

                if (!self.listening) {
                    self.query.setUp(function () {
                        self.createServer();
                    })
                }
            });
        })
        .on("error", function (err) {
            logger.error(">> Rabbit Connection Error Event " + JSON.stringify(err));
        })
//        .on("close", function (had_error) {
//            logger.error(">> Rabbit Connection Close Event. Transmission error:", had_error);
//            if (!had_error) {
//                setTimeout(function () {
//                    self.rabbit.reconnect();
//                }, 5000);
//            }
//        })
    ;
};


Server.prototype.createServer = function () {

    var self = this;

    self.server = smpp.createServer(function (session) {

        var storage = {
            state: "OPEN",
            link_status: true,
            c: 0, /* submit_sm counter. See "session.storage.interval" */
            chunks: {},
            remote_address: session.socket.remoteAddress + ":" + session.socket.remotePort,
            enquire_link_interval: setInterval(function () {

                if (!storage.link_status) {
                    logger.info(">>> link status [" + storage.state + "][" + storage.remote_address + "][" + (storage.user ? storage.user.full_id : "unknown") + "]: BROKEN > close session");
                    clearInterval(storage.enquire_link_interval);
                    session.close();
                    return;
                }

                storage.link_status = false;
                session.enquire_link();

            }, 60 * 1000)
        };

        session
            .on("close",                self.__on_close.bind(self, session, storage))
            .on('bind_transceiver',     self.__on_bind_transceiver.bind(self, session, storage))
            .on('submit_sm',            self.__on_submit_sm.bind(self, session, storage))
            .on('enquire_link',         self.__on_enquire_link.bind(self, session, storage))
            .on('enquire_link_resp',    self.__on_enquire_link_resp.bind(self, session, storage))
            .on("error", function (err) {
                logger.error(">>>>> SMPP ERROR ", err);
            })
        ;
    });

    self.server
        .on("listening", function () {
            logger.info(util.format("Start listening on %s", CONST.PORT));
            self.listening = true;

//            self.server.close();

        })
        .on("close", function () {
            logger.info(">>>>> Server 'close' event");
            self.listening = false;
        })
        .on("error", function (e) {
            logger.error("Server Error Event. Code: " + e.code);
            if (e.code == 'EADDRINUSE') {
                logger.error('Address in use, retrying...');
                setTimeout(function () {
                    self.server.listen(CONST.PORT);
                }, 3000);
            }
        })
        .listen(CONST.PORT)
    ;

};


Server.prototype.errorHandler = function(){

};


Server.prototype.__on_bind_transceiver = function (session, storage, pdu) {
    var self = this;

    if (storage.state === "BOUND_TRX") {
        session.send(pdu.response({
            command_status: smpp.ESME_RALYBND
        }));
        return;
    }

    session.pause();
    self.checkAsyncUserPass(pdu.system_id, pdu.password, function (err, res) {
        if (err) {
            logger.error("DB error");
            session.send(pdu.response({command_status: smpp.ESME_RSYSERR}));
            return;
        }
        if (!res.valid) {
            logger.error("user: " + JSON.stringify(res.user) + "\t[invalid password]");
            session.send(pdu.response({command_status: smpp.ESME_RINVPASWD}));
            session.close();
            return;
        }

        var user_id = res.user._id + "";

        self.userSession[user_id] = session;

        _.extend(storage, {
            user: _.extend(res.user, {full_id: res.user._id + "." + res.user.system_id}),
            state: "BOUND_TRX",
            interval: setInterval(function () {
                /* reset count every second */
                storage.c = 0;
            }, 1000)
        });

        session.logger = new (winston.Logger)({
            transports: [
                new (winston.transports.File)({
                    filename: path.join(__dirname, "log", storage.user.full_id + '.log'),
                    options: {
                        flags: "a"
                    },
                    level: "debug",
                    json: false,
                    timestamp: true
                })
            ]
        });

        session.send(pdu.response());
        session.resume();

        self.get_rabbit_queue(user_id, function (err, queue) {
            self.bind_user_to_queue(session, storage, queue);
        });

        logger.info('user: ' + storage.user.full_id + "\taction: add\t\t[total: " + Object.keys(self.userSession).length + "]");

    });
};


Server.prototype.__on_enquire_link = function (session, storage, pdu) {
    if (storage.state !== 'BOUND_TRX') {
        session.send(pdu.response({
            command_status: smpp.ESME_RINVBNDSTS
        }));
        return;
    }
    session.send(pdu.response());
};


Server.prototype.__on_enquire_link_resp = function (session, storage, pdu) {
    logger.info(">>> link status [" + storage.state + "][" + storage.remote_address + "][" + (storage.user ? storage.user.full_id : "unknown") + "]:\tOK");
    storage.link_status = true;
};


Server.prototype.__on_submit_sm = function (session, storage, pdu) {
    var self = this, udh = false;

    if (!pdu.source_addr.match(/^\w{1,11}$/)) {
        session.send(pdu.response({command_status: smpp.ESME_RINVSRCADR}));
        return;
    }

    if (!pdu.destination_addr.match(/^[78]9\d{9}$/)) {
        session.send(pdu.response({command_status: smpp.ESME_RINVNUMDESTS}));
        return;
    }

    storage.c += 1;
    if (storage.c > CONST.DEFAULT_LIMIT_PER_SECOND) {
        session.logger.info(">>> user: " + storage.user.full_id + '\tQueue Limit [' + storage.c + ']');
        session.send(pdu.response({command_status: CONST.ERRORS.ESME_RMSGQFUL}));
        return;
    }

    var params = {
        _id: Server.generateObjectId(),
        source_addr: pdu.source_addr,
        number: pdu.destination_addr,
        validity_period: pdu.validity_period,
        gateway_id: storage.user.gateway_id || CONST.DEFAULT_GATEWAY
    };

    if (pdu.short_message.hasOwnProperty("udh") && pdu.short_message.udh[4] > 0) {
        udh = true;

        params.short_message = pdu.short_message.message;

        var udh_data = pdu.short_message.udh,
            total = udh_data[4],
            current = udh_data[5],
            key = params.number + params.source_addr
            ;

        if (!storage.chunks.hasOwnProperty(key)) {
            storage.chunks[key] = [];
        }

        var containers = storage.chunks[key], container;

        var i = 0, len = containers.length, f = false;

        /* find container where current sms chunk is not filled */
        for (; i < len; i++) {
            container = containers[i];
            if (!container[current - 1]) {
                f = true;
                container[current - 1] = self.query.get_row(storage.user._id, storage.user.priority, params);
                break;
            }
        }

        if (!f) {
            /* create new container for key */
            container = [];
            container[current - 1] = self.query.get_row(storage.user._id, storage.user.priority, params);
            containers.push(container);
        }

        if (container.length === total && !helper.array_have_undefined(container)) {

            var message_id = Server.generateObjectId(), total_length = container.length;

            container.map(function (el, i) {
                _.extend(el, {message_id: message_id, part: i + 1, total: total_length});
                return el;
            });

            var t1 = +new Date();
            self.query.insert(container, function (err, res) {
                if (err) {
                    session.logger.error("user:" + storage.user.full_id + "\taction: create row error [" + err + "]");
                    session.send(pdu.response({command_status: smpp.ESME_RSYSERR}));
                    return;
                }

                var x = res[0], long_message = res.map(function (el) {
                    return el.short_message;
                }).join("").replace(/\n/g, "");

                session.logger.debug(util.format("user: %s\taction: save message (udh) (%s, %ss) [message_id: %s parts: %s %s %s \"%s\" gw: %s]",
                    storage.user.full_id, long_message.length, (+new Date() - t1) / 1000, message_id, total_length, x.number, x.source_addr, long_message, x.gateway_id));

                session.send(pdu.response({message_id: params._id + ""}));
            });
        }else{
            session.send(pdu.response({message_id: params._id + ""}));
        }

    } else {

        if (pdu.short_message.message) {

            params.short_message = pdu.short_message.message;

        } else if (pdu.message_payload) {

            // stolen x 2
            var encoding = pdu.data_coding & 0x0F;
            var ucs2 = encoding == 0x08; // UCS2
            var ascii = encoding == 0x01 || !encoding; // ASCII
            if (ucs2) {
                helper.le2be(pdu.message_payload);
            }
            if (ucs2 || ascii) {
                params.short_message = pdu.message_payload.toString(ucs2 ? 'ucs2' : 'ascii');
            } else {
                params.short_message = "";
            }

        } else {
            session.send(pdu.response({command_status: smpp.ESME_RSYSERR}));
            return;
        }

        var t2 = +new Date();
        self.query.insert(self.query.get_row(storage.user._id, storage.user.priority, params), function (err, res) {
            if (err) {
                session.logger.error("user:" + storage.user.full_id + "\taction: create row error [" + err + "]");
                session.send(pdu.response({command_status: smpp.ESME_RSYSERR}));
                return;
            }

            var x = res[0];

            session.logger.debug(util.format("user: %s\taction: save message [%s, %ss] [_id: %s parts: %s %s %s \"%s\" gw: %s]",
                storage.user.full_id, x.short_message.length, (+new Date() - t2) / 1000, x._id, res.length, x.number, x.source_addr, params.short_message, x.gateway_id));

            session.send(pdu.response({message_id: params._id + ""}));
        });
    }
};


Server.prototype.__on_close = function (session, storage, e) {
    logger.error(util.format(">>> close event [%s][%s]", storage.remote_address, (storage.user ? storage.user.full_id : "unknown")));

    storage.state = "CLOSED";

    if (storage.interval) {
        clearInterval(storage.interval);
    }

    clearInterval(storage.enquire_link_interval);

    if (storage.user) {
        delete this.userSession[storage.user._id];
        logger.info(util.format("active sessions (%s): %s", Object.keys(this.userSession).length, JSON.stringify(Object.keys(this.userSession))));
    }
};


Server.prototype.checkAsyncUserPass = function (system_id, password, cb) {
    this.query.get_user(system_id, function (err, user) {
        if (err) {
            cb(err);
        } else {
            if (!user) {
                cb(null, {user: null, valid: false})
            } else {
                cb(null, {user: user, valid: user.password == password && user.active})
            }
        }
    })
};


Server.prototype.get_deliver_sm_pdu = function (message, number, source_addr, message_state, receipted_message_id) {
    var params = {
        service_type: "",
        source_addr_ton: 0x00,
        source_addr_npi: 0x00,
        source_addr: number + "",
        dest_addr_ton: 1,
        dest_addr_npi: 1,
        destination_addr: source_addr + "",
        esm_class: 0x04,
        protocol_id: 0,
        priority_flag: 0,
        schedule_delivery_time: "",
        validity_period: "",
        registered_delivery: 0x00,
        replace_if_present_flag: 0x00,
        data_coding: 0x08,
        sm_default_msg_id: 0,
        short_message: "",
        message_state: message_state,
        receipted_message_id: receipted_message_id + ""
    };
    if(message){
        if (Buffer.byteLength(message, helper.ascii_or_ucs2(message)) > 140) {
            params.message_payload = message;
        } else {
            params.short_message = message;
        }
    }
    return new smpp.PDU('deliver_sm', params);
};


var server = new Server();
server.init();

//process.on('uncaughtException', function (err) {
//    logger.error("uncaughtException > ", err);
//});

exports.SmppServer = Server;