'use strict';

var assert = require('assert'),
    path = require("path"),
    util = require("util"),
    fs = require('fs'),
    events = require('events'),

    smpp = require('smpp'),
    async = require('async'),
    amqp = require('amqp'),
    _ = require('underscore'),
    winston = require('winston'),

    helper = require("./helper"),
    config = require('./config'),
    query = require('./query');

var DEBUG = true;

function App(title, cfg) {

    this.title = title;

    this.setConfigAndLogger(cfg);

    // separated messages
    this.chunks = {};

    this.__submit_sm_queue = [];

    /* rabbit mq exchanges */
    this.exchange = {};

    this.test_mode = false;

    this.CONST = {
        STATUS: {
            fresh:0, // new message in db
            progress:1, // get from db for populate memory queue
            sent:2, // pdu received. Command status == 0
            failed:3 // pdu received. Unknown command status.
//            block:4 // pdu received.
        }
    };

    this.__last_enquire = new Date();

    this.stats = {
        submit_sm: {
            send: 0,
            sent: 0,
            full_messages: 0,
            updated: 0,
            error: 0
        },
        deliver_sm: {
            received: 0,
            updated: 0
        }
    };
    this.errors_counter = 0;

    this.query = new query.MongoQuery(this.cfg.query);

    this.state = '';

    this.service = [{name: 'db', status: -1}, {name: 'rabbit', status: -1}, {name: 'smpp', status: -1}];
    this.__service_scheduled_to_restart = {};
}

util.inherits(App, events.EventEmitter);


App.EXCHANGE = {
    DELIVER_SM: "deliver_sm",
    SUBMIT_SM_RESP: "submit_sm_resp"
};

App.SHORT_MESSAGE_MAX_BYTES = 140;

App.prototype.getLogger = function(){

    var transports = [
        new (winston.transports.File)({
            filename: path.join(__dirname, "log", this.title + ".log"),
            options: {
                flags: "a"
            },
            level: this.cfg.general.level || "debug",
            json: false,
            timestamp: true
        })
    ];

    if(DEBUG){
        transports.push(new winston.transports.Console({ level: "debug", json: false, timestamp: true }))
    }

    return new (winston.Logger)({
        transports: transports
    });
};

App.__make_short_message_with_udh = function(buffer, part, total){
    var udh = new Buffer([0x05, 0x00, 0x03, 0x00, total, part]),
        res = new Buffer(udh.length + buffer.length);

    udh.copy(res);
    buffer.copy(res, udh.length);
    return res;
};


App.__split_long_message_with_udh = function(buffer){
    var start, end, chunk, total = Math.ceil(buffer.length/134), res = [];

    for(var i = 0; i < total; i++){
        start = i*134;
        end = ((start+134) > buffer.length) ? buffer.length : start+134;

        chunk = buffer.slice(start, end);

        res.push(App.__make_short_message_with_udh(chunk, i+1, total));
    }

    return res;
};


App.make_buffer = function(text, encoding, le2be){
    encoding = encoding || helper.ascii_or_ucs2(text);
    var buffer = new Buffer(text, encoding);
    if(le2be){
        helper.le2be(buffer);
    }
    return buffer;
};


App.prototype.init = function(){
    this.on('service', this._onServiceMessage.bind(this));
//    this._scheduleToRestart(['db', 'rabbit', 'smpp']);
    this._scheduleToRestart(['smpp']);
};

App.prototype._onServiceMessage = function(message){

    var service = this.service.filter(function(x){ return x.name === message.name })[0];
    service.status = message.status;

    this.__service_scheduled_to_restart[message.name] = false;

    if(message.status === 0){ // service down

        this.stopPollingAndPopulate();

        if(service.name === 'rabbit'){
            this._initRabbit();
        }else if(service.name === 'db'){
            this._initDb();
        } else if (service.name === 'smpp'){
            this._initSmpp();
        }

        if(this.session){
            this.session.pause();
        }

    } else if (message.status === 1) { // service up

        var allReady = this.service.every(function(x){ return x.status === 1 });

        if(allReady){
            this._onServicesReady();
        }
    }
};

App.prototype._initDb = function(){
    if(this.query){
        this.query.destroy();
    }

    this.logger.info('[db] connecting to %s:%s/%s', this.query.cfg.host, this.query.cfg.port, this.query.cfg.dbname);

    this.query.setUp(this._onQueryModuleReady.bind(this));
};

App.prototype._initSmpp = function(){

    if(this.session){
        this.session.close();
        this.session.socket.destroy();
    }

    this.logger.info("[smpp] connecting to " + this.cfg.connection.host + ":" + this.cfg.connection.port);

    this.session = smpp.createSession(this.cfg.connection.host, this.cfg.connection.port);

    this.session.socket.setTimeout(60 * 1000, this._onSmppSocketTimeout.bind(this));

    this.session
        .once('close',        this.__on_close.bind(this))
        .on('connect',      this.__on_connect.bind(this))
        .on('error',        this.__on_error.bind(this))
        .on('enquire_link', this.__on_enquire_link.bind(this))
        .on('unknown',      this.__on_unknown.bind(this))
        .on('deliver_sm',   this.__on_deliver.bind(this))
    ;
};

App.prototype._onSmppSocketTimeout = function(){
    this.logger.error('[smpp] socket timeout');
    this.session.socket.end();
    this.session.socket.destroy();
};

App.prototype._initRabbit = function(){

    if(this.rabbit){
        this.rabbit.destroy();
        this.rabbit = null;
    }

    this.logger.info(util.format("[rabbit] connecting to %s:%s%s",
        config.rabbit.connection.host, config.rabbit.connection.port, config.rabbit.connection.vhost));

    this.rabbit = amqp.createConnection(config.rabbit.connection, config.rabbit.options, this._onRabbitConnectionReady.bind(this));

    this.rabbit
        .on('error', this.logger.error.bind(this.logger, "[rabbit] connection error event: "))
        .once('close', this._onRabbitClose.bind(this))
    ;
};

App.prototype._scheduleToRestart = function(services, timeout){

    this.logger.info(util.format('[general] scheduled to restart %s - %s',
        services.join('|'), (timeout === undefined) ? 'immediately' : 'through ' + timeout + 'ms'));

    for(var i = 0, len = services.length; i < len; i++){
        var service = services[i];
        if(!this.__service_scheduled_to_restart[service]){

            this.__service_scheduled_to_restart[service] = true;

            if(timeout === undefined){
                this.emit('service', {name: service, status: 0});
            } else {
                setTimeout(this.emit.bind(this, 'service', {name: service, status: 0}), timeout);
            }

        }
    }
};

App.prototype._onServicesReady = function(){

    this.logger.info('[general] all services ready');

    if(this.__i1){
        clearInterval(this.__i1);
        this.__i1 = null;
    }

    if(this.__i2){
        clearInterval(this.__i2);
        this.__i2 = null;
    }

    var interval = 60;

    this.session.resume();

    this.__i1 = setInterval(function () {
        if (this.state === 'BOUND_TRX') {

            // no response last 5 minutes
            if(new Date() - this.__last_enquire > 5 * 60 * 1000){
                return this._scheduleToRestart(['smpp']);
            }

            /* ugly */
            this.session.resume();

            this.session.enquire_link();

            if(helper.exists_non_zero(this.stats)){
                this.logger.info(util.format('[general] last %ss > %s heap: %skb',
                    interval, JSON.stringify(this.stats), Math.round(process.memoryUsage().heapUsed / 1024)));
            }

            helper.set_zero(this.stats);
        }
    }.bind(this), interval * 1000);

    this.__i2 = setInterval(function () {
        if (this.state !== 'BOUND_TRX') return;

        if (this.remote_server.is_ok()) {
            if (this.test_mode) this.toggle_test_mode();
        } else {
            if (!this.test_mode) this.toggle_test_mode();
        }
    }.bind(this), 5 * 1000);

    this.startPollingAndPopulate();
};

App.prototype._onRabbitClose = function(had_error){
    var timeout = 10;
    this.logger.error(util.format("[rabbit] connection close. Transmission error: %s. Restart through %ss", had_error, timeout));

    this._scheduleToRestart(['rabbit'], timeout * 1000);
};

App.prototype._onRabbitConnectionReady = function(){

    var exchange_list = [
        [App.EXCHANGE.DELIVER_SM, {type:'topic', autoDelete: false, durable: true, confirm: true}],
        [App.EXCHANGE.SUBMIT_SM_RESP, {type: "topic", autoDelete: false, durable: true}]
    ];

    var func_list = exchange_list.map(function(rabbit, x){
        var exchange_name = x[0], params = x[1];

        return function(callback){
            rabbit.exchange(exchange_name, params, function (exchange) {
                callback(null, exchange);
            });
        }
    }.bind(this, this.rabbit));

    async.parallel(func_list, function(err, res){

        for(var i = 0, len = exchange_list.length; i < len; i++){
            var exchange_name = exchange_list[i][0];
            this.exchange[exchange_name] = res[i];
            this.logger.info("[rabbit]\texchange " + exchange_name + ": ready")
        }

        // rabbit ready
        this.emit('service', {name: 'rabbit', status: 1});

    }.bind(this));

};

App.prototype._onQueryModuleReady = function(err, res){

    if (err){
        var timeout = 10;
        this.logger.error("[db] setup failed (%s). Restart through %ss", JSON.stringify(err, ['message']), timeout);
        return this._scheduleToRestart(['db'], timeout * 1000);
    }

    // db ready
    this.emit('service', {name: 'db', status: 1});
};

App.prototype.setConfigAndLogger = function(value){
    this.cfg = value;
    this.logger = this.getLogger();
};

App.prototype.toggle_test_mode = function(value){
    this.test_mode = value || !this.test_mode;
    this.logger.info("Test mode: %s >> Restart Polling", ~~this.test_mode);
    this.stopPollingMemoryQueue();
    this.pollingMemoryQueue();
};

App.prototype.__on_close = function(e){

    this.logger.error(util.format('[smpp] connection closed > %s', e));
    this.state = 'CLOSED';

    if(this.errors_counter < 60){
        this.errors_counter += 1;
    }

    this._scheduleToRestart(['smpp'], this.errors_counter * 1000);

    this.logger.info("[smpp] trying to reconnect through " + this.errors_counter + "s");
};

App.prototype.__on_connect = function(){
    this.logger.info("[smpp] connection successfully established. Send bind_transceiver.");
    this.state = "OPEN";

    this.session.bind_transceiver(this.cfg.pdu.bind_transceiver, this._onBindTransceiverResp.bind(this));
};

App.prototype._onBindTransceiverResp = function (pdu) {
    if (pdu.command_status === 0) {

        this.logger.info("[smpp] bind transceiver successfully");
        this.state = "BOUND_TRX";

        this.session.pause();

        this.errors_counter = 0;
        // smpp ready
        this.emit('service', {name: 'smpp', status: 1});
    } else {
        this.logger.error(util.format("[smpp] bind transceiver failed. Command status: %s > close session", pdu.command_status));
    }
};


App.prototype.__on_deliver = function (pdu) {
    this.logger.debug(util.format("[smpp] deliver_sm: %s %s %s", pdu.receipted_message_id, pdu.destination_addr, pdu.message_state));
    this.stats.deliver_sm.received += 1;
    this.session.send(pdu.response());

    this.query.update_after_deliver_sm(this.cfg.general.gateway_id, pdu.receipted_message_id, pdu.message_state,
        new Date(), {destination_addr: pdu.destination_addr, source_addr: pdu.source_addr}, this._onDeliverSmUpdated.bind(this, {pdu: pdu}));
};

App.prototype._onDeliverSmUpdated = function (args, err, row) {
    if (err) {
        this.logger.error(util.format("[db] update after deliver_sm: %s", err));
        return this._scheduleToRestart(['smpp', 'db', 'rabbit']);
    }

    if(this.exchange[App.EXCHANGE.DELIVER_SM].state !== 'open') return this._scheduleToRestart(['rabbit']);

    var routing_key = (row.user_id || "0") + "." + row.message_state + "." + (row.via || "0");

    this.logger.verbose(util.format("[rabbit] deliver_sm: %s sms_id:%s %s %s %s",
        row.remote_id, row.sms_id, routing_key, args.pdu.destination_addr, args.pdu.source_addr));

    this.exchange[App.EXCHANGE.DELIVER_SM].publish(routing_key, row);

    this.stats.deliver_sm.updated += 1;
};


App.prototype.__on_error = function(err){
    this.logger.error("[smpp] %s", JSON.stringify(err, ['message']));
//    this.stopPollingAndPopulate();
//    this.session.close();
};

App.prototype.__on_unknown = function (e) {
    this.logger.error("Unknown: %s", JSON.stringify(e));
    this.stopPollingAndPopulate();
};

App.prototype.__on_enquire_link = function (pdu) {
    this.__last_enquire = new Date();
    this.session.send(pdu.response());
};

App.prototype.remote_server = {
    __i:0,
    is_ok:function () {
        return !!(this.__i < 200);
    },
    inc:function () {
        this.__i += 1;
    },
    set_ok:function () {
        this.__i = 0;
    },
    get_count:function () {
        return this.__i;
    }
};

App.prototype.startPollingAndPopulate = function () {
    this.populateMemoryQueue();
    this.pollingMemoryQueue();
};

App.prototype.stopPollingAndPopulate = function () {
    this.stopPollingMemoryQueue();
    this.stopPopulateMemoryQueue();
    return this;
};

App.prototype.stopPollingMemoryQueue = function () {
    if (this.__p1) {
        clearInterval(this.__p1);
        this.__p1 = null;
    }
};

App.prototype.pollingMemoryQueue = function () {
    var queue_length, polling_interval;

    if (this.test_mode) {
        queue_length = 1;
        polling_interval = 10 * 1000;
    } else {
        queue_length = this.cfg.general.queue_length;
        polling_interval = 100;
    }

    this.__p1 = setInterval(this._pollingMemory.bind(this, {queue_length: queue_length}), polling_interval);
};

App.prototype._pollingMemory = function(args){
    if (!this.__submit_sm_queue.length) {
        this.stopPollingMemoryQueue();
        return;
    }

    var x;
    for (var i = 0; i < Math.ceil(args.queue_length / 10); i++) {
        x = this.__submit_sm_queue.shift();
        if(x) {
            this.session.submit_sm(x.value, this.handle_submit_sm_resp.bind(this, x.meta.row, x.meta.memory_split, x.meta.latest_part));
        }else{
            break;
        }
    }
};

App.prototype.stopPopulateMemoryQueue = function () {
    this.logger.info("[general] stopped populate memory queue");
    clearInterval(this.__p2);
    this.__p2 = null;
};

App.prototype.populateMemoryQueue = function () {
    var self = this, f = true;

    var next_query_limit = this.cfg.query.query_limit, interval = 3;

    this.__p2 = setInterval(function () {

        if (!f) return;

        // memory queue is full
        if (this.__submit_sm_queue.length > next_query_limit) return this.logger.debug("Parts in memory: " + self.__submit_sm_queue.length + " (" + next_query_limit + ")");

        f = false;
        var begin = +new Date();
        this.query.get_and_update(this.cfg.general.gateway_id, function (err, rows) {

            if (err){
                this.logger.error(util.format('[db] getting rows > restarting): %s', JSON.stringify(err)));
                return this._scheduleToRestart(['db']);
            }

            f = true;
            var end = new Date().getTime();

            if(rows.length){
                for(var i = 0, ii = rows.length; i < ii; i++){
                    this.__divide(rows[i]);
                }

                if(!this.__p1){
                    this.pollingMemoryQueue();
                }

                this.logger.info("[db] query completed in " + ((end-begin)/1000) + " s. Rows in memory: " + self.__submit_sm_queue.length);

            }
        }.bind(this));

    }.bind(this), interval*1000);

};

/* custom handlers for command_status != 0 */
App.prototype.status_handlers = [

];

App.prototype.handle_submit_sm_resp = function(row, split_in_memory, latest_part, pdu){
    var remote_id, status;

    this.logger.debug(util.format("[smpp] submit_sm_resp remote_id: %s _id: %s %s", pdu.message_id, row._id, row.source_addr));

    this.stats.submit_sm.sent += 1;
    this.remote_server.set_ok();

    if (pdu.command_status === 0) {
        remote_id = pdu.message_id;
        status = this.CONST.STATUS.sent;
    } else {
        remote_id = null;
        status = this.CONST.STATUS.failed;
        this.stats.submit_sm.error += 1;
        this.logger.log("error", "[smpp] submit_sm_resp[failed] > command_status: %j %j", pdu.command_status, row, {});
    }

    if(remote_id){
        this.query.insertRemoteId({
            sms_id:     row._id,
            remote_id:  remote_id,
            sent_at: new Date(),
            gateway_id: this.cfg.general.gateway_id,
            user_id: row.user_id,
            via: row.via || 0
        }, function(err){

            if(err) return this._scheduleToRestart(['db']);

            this.stats.submit_sm.updated += 1;
        }.bind(this));
    }

    var routingKey;
    if(!split_in_memory || (split_in_memory && latest_part) || !remote_id){

        routingKey = "fully";

        this.query.update_after_submit_sm_resp({_id: row._id, status: status}, function (err) {
            if (err) return this._scheduleToRestart(['db']);

            if(latest_part){
                this.stats.submit_sm.full_messages += 1;
            }
        }.bind(this));
    } else {
        routingKey = "";
    }

    this.exchange[App.EXCHANGE.SUBMIT_SM_RESP].publish(routingKey, {row: row, pdu: pdu});
};

App.prototype.__get_data_coding = function(encoding){
    var value = this.cfg.data_coding && this.cfg.data_coding[encoding];
    if(value !== undefined){
        return value;
    }else{
        return smpp.consts.ENCODING[encoding.toUpperCase()];
    }
};

App.prototype.__divide = function (row) {
    var self = this, text = "", i = 0, encoding;

    self.remote_server.inc();

    /* split message */
    if(row.message_id){
        if(!self.chunks.hasOwnProperty(row.message_id)){
            self.chunks[row.message_id] = [];
        }
        self.chunks[row.message_id][row.part-1] = row;

        if(self.chunks[row.message_id].length !== row.total || helper.array_have_undefined(self.chunks[row.message_id])){
            return;
        }

        var chunks = self.chunks[row.message_id];

        text = chunks.map(function(el){ return el.short_message; }).join("");

        encoding = helper.ascii_or_ucs2(text);

        params = {
            destination_addr: row.number + "",
            source_addr: row.source_addr,
            data_coding: self.__get_data_coding(encoding),
            registered_delivery: 1,
            dest_addr_ton:1,
            dest_addr_npi:1,
            source_addr_ton:5,
            source_addr_npi:0,
            validity_period: "000000020015000R"
        };

        if (row.validity_period) {
            params.validity_period = row.validity_period;
        }

        for(; i < chunks.length; i++){

            _.extend(params, self.cfg.pdu.submit_sm, {
                short_message: App.__make_short_message_with_udh(App.make_buffer(chunks[i].short_message, encoding, encoding === "ucs2"), i+1, row.total),
                esm_class: params.esm_class | smpp.consts.ESM_CLASS.UDH_INDICATOR,
                data_coding: self.cfg.pdu.submit_sm.data_coding || smpp.consts.ENCODING[encoding.toUpperCase()]
            });

            this.__submit_sm_queue.push({value: params, meta: {row: row}});

        }
        return;

    }

    text = row.short_message;
    encoding = helper.ascii_or_ucs2(text);

    var text_byte_length = Buffer.byteLength(text, encoding);

    var params = {
        destination_addr: row.number + "",
        source_addr: row.source_addr,
        data_coding: self.__get_data_coding(encoding),
        registered_delivery: 1,
        dest_addr_ton:1,
        dest_addr_npi:1,
        source_addr_ton:5,
        source_addr_npi:0,
        validity_period: "000000060015000R"
    };

    if (row.validity_period) {
        params.validity_period = row.validity_period;
    }

    if(text_byte_length <= App.SHORT_MESSAGE_MAX_BYTES){

        self.stats.submit_sm.send += 1;

        params.short_message = App.make_buffer(text, encoding, encoding === "ucs2");

        this.__submit_sm_queue.push({value: params, meta: {row: row}});

    }else{

        params.esm_class = params.esm_class | smpp.consts.ESM_CLASS.UDH_INDICATOR;

        var short_message_buffers = App.__split_long_message_with_udh(App.make_buffer(text, encoding, encoding === "ucs2")),
            len = short_message_buffers.length;

        for(i = 0; i < len; i++){
            params.short_message = short_message_buffers[i];

            self.stats.submit_sm.send += 1;

            (i+1 === len)
                ? self.__submit_sm_queue.push({value: _.clone(params), meta: {row: row, memory_split: true, latest_part: true}})
                : self.__submit_sm_queue.push({value: _.clone(params), meta: {row: row, memory_split: true}})
            ;
        }
    }
};

App.prototype.graceful_shutdown = function(cb){
    var self = this;
    self.stopPollingAndPopulate();

    if(self.__submit_sm_queue.length){
        var _ids = [];
        for(var i = self.__submit_sm_queue.length; i--;){
            _ids.push(self.__submit_sm_queue[i].value._id);
        }
        self.logger.info("update status > " + JSON.stringify(_ids));
        self.query.get_db().collection("active").update({_id: {"$in": _ids}}, {"$set": {status: 0}}, {w: 1, multi: true}, function(err, res){
            cb();
        });

    }else{
        cb();
    }
};

if (process.argv.length !== 3) {
    process.stdout.write(util.format("Usage: %s \<config\>\n", __filename));
    return false;
}

function readConfig(name) {
    var config_path = path.join(__dirname, "config", name);
    delete require.cache[require.resolve(config_path)];
    return require(config_path);
}

var cfg_title = process.argv[2];
var app = new App(cfg_title, readConfig(cfg_title));
app.init();

process.on('SIGTERM', function() {
    app.graceful_shutdown(function(){
        process.exit();
    });
});

process.on('SIGHUP', function() {
    app.setConfigAndLogger(readConfig(cfg_title));
    app.toggle_test_mode(true);
});