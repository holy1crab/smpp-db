
exports.general = {
    queue_length: 30,
    gateway_id: 10,
    udh: true
};

exports.connection = {
    host: '54.213.199.248',
    port: 4442
};

exports.query = {
    host: 'localhost',
    dbname:'sms',
    collection:'active',
    username:'sms',
    password:'1sms',
    query_limit: 300
};

exports.pdu = {
    bind_transceiver: {
        system_id: 'abc',
        password: 'cde',
        addr_ton:1,
        addr_npi:1,
        address_range:''
    }
};


