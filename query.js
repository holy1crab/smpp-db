var log = require('./helper').log_with_date,
    mongo = require('mongodb'),
    BSON = mongo.BSONPure,
    MongoClient = require('mongodb').MongoClient,
    _ = require('underscore'),
    async = require('async'),
    util = require('util'),
    assert = require('assert')
    ;



exports.MongoQuery = MongoQuery;

var CONST = {
    DUPLICATE_KEY: 11000
};

function MongoQuery(o) {

    this.cfg = {
        query_limit:1000,
        host:'localhost',
        port:27017,
        dbname:'sms',
        username:'',
        password:''
    };

    _.extend(this.cfg, o);

    this.f = true;
    this.__first_time = true;
    this._cache = {};
}

MongoQuery.ObjectId = function(hex){
    return new BSON.ObjectID(hex);
};

MongoQuery.prototype.createConnection = function(opts, cb){
    var self = this;
    MongoClient.connect(util.format('mongodb://%s:%s/%s', self.cfg.host, self.cfg.port, self.cfg.dbname), {
        db:{
            native_parser:false,
            w:1,
            wtimeoutMS: 5000,
            journal:false,
            fsync:false
        },
        server: {
            auto_reconnect: false,
            poolSize: opts.poolSize || 16
        }
    }, function (err, db) {
        if(err) return cb(err);

        if(self.cfg.username){
            db.authenticate(self.cfg.username, self.cfg.password, function (err, is_auth) {
                if(err) return cb(err);

                cb(null, db);
            });
        } else {
            cb(null, db);
        }
    })
};


MongoQuery.prototype.destroy = function(){
    if(this.db){
        this.db.close();
    }

    if(this.db2){
        this.db2.close();
    }

    this.db = null;
    this.db2 = null;
};


MongoQuery.prototype.setUp = function (cb) {
    var self = this;

    async.parallel([
        function(callback){
            self.createConnection({poolSize: 3}, callback);
        },
        function(callback){
            self.createConnection({poolSize: 32}, callback);
        }
    ], function(err, res){
        if(err) return cb(err);

        self.db2 = res[0];
        self.db = res[1];

        async.parallel([
            function(callback){
                self.db.collection("active").ensureIndex({status: 1, gateway_id: 1, priority: -1, _id: 1}, callback);
            },
            function(callback){
                self.db.collection("status").ensureIndex({remote_id: 1, gateway_id: 1}, callback);
            }
        ], function(err, res){
            if(err) return cb(err);

            // prevent double callback if database crashes
            if(self.__first_time){
                cb(null, null);
                self.__first_time = false;
            }
        })
    });
};

MongoQuery.prototype.get_db = function () {
    return this.db;
};

MongoQuery.prototype.get_user = function (system_id, cb) {
    this.db.collection('user').findOne({system_id:system_id}, cb);
};

MongoQuery.prototype.get_row = function (user_id, priority, parameters) {

    var row = {
        created_at:             parameters.created_at || new Date(),
        status:                 0,
        number:                 parameters.number,
        short_message:          parameters.short_message,
        source_addr:            parameters.source_addr,
        user_id:                user_id,
        gateway_id:             parameters.gateway_id || 3,
        priority:               priority || parameters.priority || 2
    };

    if (parameters.validity_period) {
        row.validity_period = parameters.validity_period;
    }

    if(parameters._id) {
        row._id = parameters._id;
    }

    return row;
};

MongoQuery.prototype.insert = function(rows, cb){
    this.db.collection("active").insert(rows, {}, cb);
};


MongoQuery.prototype.explain = function (gw, cb) {
    var self = this, col = self.db.collection('queue');
    log("query.start");
    col.find({status:0, gateway_id:gw}, {
        limit:self.cfg.query_limit,
        sort:[
            ['priority', -1],
            ['_id', 1]
        ],
        hint:  {gateway_id: 1, status: 1, priority: -1, _id: 1}
    }).explain(cb);

};

MongoQuery.prototype._cachedCollection = function(name, db){
    db || (db = this.db);
    if(this._cache.hasOwnProperty(name)){
        return this._cache[name];
    }else{
        var collection = db.collection(name);
        this._cache[name] = collection;
        return collection;
    }
};

/* Client queries */
MongoQuery.prototype.get_and_update = function (gateway_id, cb) {

    var collection = this._cachedCollection('active', this.db2);

    collection.find({status:0, gateway_id: gateway_id}, {
        limit:this.cfg.query_limit,
        sort:[
            ['priority', -1],
            ['_id', 1]
        ],
        hint:  {status: 1, gateway_id: 1, priority: -1, _id: 1}
    }).toArray(this._onBlankRowsReceived.bind(this, {callback: cb, collection: collection}));
};

MongoQuery.prototype._onBlankRowsReceived = function (args, err, rows) {

    if (err) return args.callback(err);

    if (!rows.length) return args.callback(null, rows);

    var _ids = [], i;
    for(i = rows.length; i--;){
        _ids.push(rows[i]._id);
    }

    args.rows = rows;

    args.collection.update({_id: {$in: _ids}}, {$set: {status: 1}}, {w: 1, multi: true}, this._onBlankRowsUpdated.bind(this, args));
};

MongoQuery.prototype._onBlankRowsUpdated = function(args, err){
    (err) ? args.callback(err) : args.callback(null, args.rows);
};

MongoQuery.prototype.update_after_deliver_sm = function (gateway_id, remote_id, message_state, delivery_time, payload, cb) {
    var term = {remote_id:remote_id + "", gateway_id: gateway_id},
        collection = this._cachedCollection('status');

    collection.findAndModify(term, [], {'$set':{
        remote_id: remote_id,
        gateway_id: gateway_id,
        message_state: message_state,
        message_state_received_at: delivery_time,
        payload: payload
    }}, {
        w: 1,
        new: true,
        hint: {remote_id: 1, gateway_id: 1},
        upsert: true
    }, this._onStatusUpdated.bind(this, {callback: cb}));
};

MongoQuery.prototype._onStatusUpdated = function(args, err, item){

    if(err) return args.callback(err);

    if(!item.sms_id) return args.callback(null, item);

    this._cachedCollection('active').findOne({_id: item.sms_id}, {_id: 0, source_addr: 1, short_message: 1, number: 1, payload: 1}, function(err2, item2){
        if(err2) return args.callback(err2);

        args.callback(null, _.extend(item, item2));
    })
};


/*
MongoQuery.prototype.transfer = function(row, cb){
    var self = this;
    self.db.collection("finalized").insert(row, {w: 1}, function(err, res){
        if(err && err.code !== CONST.DUPLICATE_KEY){
            cb(err, null);
        } else {
            self.db.collection("queue").remove({_id: row._id}, {w: 0});
            cb(null, true);
        }
    });
};
*/


MongoQuery.prototype.update_after_submit_sm_resp = function (opts, cb) {
    this._cachedCollection('active').update({_id: opts._id}, {'$set':{status:opts.status, sent_at:new Date()}}, {w: 1}, cb);
};


MongoQuery.prototype.insertRemoteId = function(opts, cb){
    this._cachedCollection('status').insert(opts, {w: 1}, cb);
};

/* Server Queries */

/*
MongoQuery.prototype.get_deliver = function (user_id, cb) {
    this.db.collection('deliver_sm').find({user_id:user_id}, {sort:[
        ['_id', 1]
    ], limit:100}).toArray(cb);
};
*/
