

var helper = {
    ascii_or_ucs2: function (text) {
        return (text.match(/[^\x00-\x7E]/)) ? "ucs2" : "ascii";
    },
    exists_non_zero: function(obj){

        var f = false;

        !function _r(obj){
            for(var i in obj){
                if(!f){
                    if (typeof obj[i] === "object"){
                        _r(obj[i])
                    }else{
                        if(obj[i] !== 0){
                            f = true;
                            return;
                        }
                    }
                }
            }
        }(obj);

        return f;
    },

    set_zero: function (obj) {
        for (var i in obj) {
            if (typeof obj[i] === "object") {
                arguments.callee(obj[i])
            } else {
                obj[i] = 0;
            }
        }
    },
    array_have_undefined: function (arr) {
        for (var i = arr.length; i--;) {
            if (arr[i] === undefined) {
                return true;
            }
        }
        return false;
    },
    le2be: function (buffer) {
        // Converts little endian to big endian or vice versa.
        for (var i = 0; i < buffer.length; i += 2) {
            var tmp = buffer[i];
            buffer[i] = buffer[i + 1];
            buffer[i + 1] = tmp;
        }
    },
    log_with_date: function () {
        var a = [];
        a.push(helper.getTime(new Date()) + "\t");
        for (var i in arguments) {
            a.push(arguments[i]);
        }
        console.log.apply(this, a);
    },
    proxy: function (fn, scope) {
        return function () {
            return fn.apply(( scope || global ), Array.prototype.slice.call(arguments));
        };
    },

    double_digits: function (v) {
        return (v + "").length == 1 ? "0" + v : v;
    },

    getTime: function (date) {
        return date.getFullYear() + '.' + (date.getMonth() + 1)
            + '.' + date.getDate() + ' ' + helper.double_digits(date.getHours())
            + ':' + helper.double_digits(date.getMinutes())
            + '.' + helper.double_digits(date.getSeconds()) + '.' + date.getMilliseconds();
    }
};


module.exports = helper;